FROM python:3.7-alpine

LABEL author=nikolai@oellegaard.com

# cache dependencies
RUN mkdir /app
COPY Pipfile /app
#COPY Pipfile.lock /app
WORKDIR /app

RUN pip install pipenv
RUN pipenv install 
#--system --deploy

# copy app
COPY . /app
ENTRYPOINT [ "pipenv", "run" ]
CMD ["pytest", "--junitxml=report.xml", "--cov", "app/"]
