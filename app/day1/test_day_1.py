from .day_1 import InputParser
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_blocks_away_from_easter_bunny_hq():
    input_parser = InputParser(file_name)
    final_destination = input_parser.get_final_position()
    assert (final_destination.x+final_destination.y) == 242


def test_first_location_visited_twice():
    input_parser = InputParser(file_name)
    position_visited_twice = input_parser.get_first_position_visited_twice()
    assert (position_visited_twice.x+position_visited_twice.y) == 150
