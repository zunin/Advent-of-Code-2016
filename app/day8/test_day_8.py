from .day_8 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_lit_pixels():
    parser = InputParser(file_name)

    encoder = CommandScreenEncoder(Screen())

    for command in parser.commands:
        encoder.parse_command(command)

    assert encoder.get_lit_pixels() == 116


def test_code_on_screen():
    parser = InputParser(file_name)

    encoder = CommandScreenEncoder(Screen())

    for command in parser.commands:
        encoder.parse_command(command)
    assert encoder.screen.__repr__() == "#..#.###...##....##.####.#....###...##..####.####.\n" + \
                                        "#..#.#..#.#..#....#.#....#....#..#.#..#.#.......#.\n" + \
                                        "#..#.#..#.#..#....#.###..#....###..#....###....#..\n" + \
                                        "#..#.###..#..#....#.#....#....#..#.#....#.....#...\n" + \
                                        "#..#.#....#..#.#..#.#....#....#..#.#..#.#....#....\n" + \
                                        ".##..#.....##...##..#....####.###...##..####.####."
