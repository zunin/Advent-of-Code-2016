from .day_6 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")



def test_error_corrected_message():
    parser = InputParser(file_name)

    decrypter = InputDecrypter()

    message = [decrypter.get_most_common_letter(column) for column in parser.columns]

    assert "".join(message) == "qtbjqiuq"


def test_original_error_corrected_message():
    parser = InputParser(file_name)

    decrypter = InputDecrypter()

    message = [decrypter.get_least_common_letter(column) for column in parser.columns]

    assert "".join(message) == "akothqli"
