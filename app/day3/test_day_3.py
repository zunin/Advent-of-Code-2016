from .day_3 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_squares_with_three_columns():
    parser = InputParser(file_name)

    valid_triangles = 0
    for triangle in parser.triangles:
        if triangle.is_valid():
            valid_triangles += 1

    assert valid_triangles == 917


def test_squares_with_three_vartical_columns():
    parser = InputParser(file_name)

    valid_triangles = 0
    for triangle in parser.column_triangles:
        if triangle.is_valid():
            valid_triangles += 1

    assert valid_triangles == 1649
