from .day_7 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_number_of_ips_supporting_tls():
    parser = InputParser(file_name)

    tls_ip_addresses = 0
    for address in parser.ip_addresses:
        if Validator.validate_with_abba(address):
            tls_ip_addresses += 1

    assert tls_ip_addresses == 110


def test_number_of_ips_supporting_ssl():
    parser = InputParser(file_name)

    ssl_ip_addresses = 0
    for address in parser.ip_addresses:
        if Validator.validate_with_ssl(address):
            ssl_ip_addresses += 1

    assert ssl_ip_addresses == 242
