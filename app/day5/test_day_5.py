from .day_5 import MD5Hasher

class RainbowTabledHasher(MD5Hasher):
    def __init__(self):
        super().__init__("ugkcyxxp")
        self.hash_cache = {
            702868: "00000d82520c9edae5aff0c95131fa2f",
            1776010: "0000043e8f3f991deb83a48397ae1e7e",
            8421983: "00000c6ef1f544e7423e5234fc295e2c",
            8744114: "00000db7c2dd1100436ce9ee71df4a6f",
            8845282: "000002c7d4257f42319af0bf3d255eac",
            9268910: "00000e94d4c249f954b637a1dc5eaa67",
            9973527: "00000e1355a341447c1b13967dacdfeb",
            10253166: "0000012e0248931afa8e7a3810442e98",
            13087891: "00000934d91223d4d08ac082fd5e4daa",
            13176820: "0000050aea0f692b0409740c1b7c021b",
            13365567: "00000a07a697603ede6bb01c8339fb67",
            13604912: "00000751932293e0a91438646f8f6117",
            14035852: "00000d2e6b26e7dc290ada3f0495833e",
            14375158: "000001f21947ff4e4e25b53c52120c7b",
            14375655: "000000f59a581a0ab3896663bf5bee0a",
            14578671: "000006e2abaa3d7bff2e8e6a35e078a6",
            15164392: "0000085df60bd737746e6a4ffb3c9813",
            17211495: "000001957386b857a3c705c49451abfe",
            18519531: "0000040fb6ee64569a3d1a37b095c693",
            18526597: "0000001de67551a31f5f57730ff131ba",
            22982317: "00000594bbcb656da7164e5f5311c035",
            23855287: "00000667a02297abc5f1947706802bcf",
            24602100: "00000e56f61e6b057183f25df9131e6a",
            24760816: "000001cd16fe4aed4ce73c87acf3ccef",
            25176241: "0000037455348b6f39fa24219c96c6f4"
        }

hasher = RainbowTabledHasher()


def test_find_first_door():
    solution_1 = hasher.find_password()
    assert solution_1 == "d4cd2ee1"


def test_sector_of_north_pole_object_room():
    solution_2 = hasher.find_password_alternate()
    assert solution_2 == "f2c730e5"
