import hashlib
from app.common import BaseInputParser
from typing import Dict, Iterable
import time


class InputParser(BaseInputParser):
    @property
    def secret(self):
        for line in self.lines:
            return line


class MD5Hasher:
    secret: str
    prefix: int
    prefix_length: int
    password_length: int
    hash_cache: Dict[int, str]


    def __init__(self, secret, prefix=0, prefix_length=5, password_length=8):
        self.secret = secret
        self.prefix = prefix
        self.prefix_length = prefix_length
        self.password_length = password_length
        self.hash_cache = {}

    def get_hash_for_index(self, index):
        encoded_string = f"{self.secret}{index}".encode()
        return hashlib.md5(encoded_string).hexdigest()

    def hashes_with_prefix(self) -> Iterable[str]:
        index = 0

        if len(self.hash_cache.keys()) > 0:
            for cached_index in sorted(self.hash_cache.keys()):
                yield self.hash_cache[cached_index]
                index = cached_index + 1

        #t_end = time.time() + 60 * 2
        
        while True: #time.time() < t_end:
            hash_candiate = self.get_hash_for_index(index)
            if hash_candiate.startswith(self.prefix_length*str(self.prefix)):
                self.hash_cache[index] = hash_candiate
                yield hash_candiate
            
            index = index + 1

    def find_password(self) -> str:
        part_1_password = []
        hash_generator = self.hashes_with_prefix()
        for valid_hash in (hash_generator.__next__() for _ in range(self.password_length)):
            part_1_password.append(valid_hash[self.prefix_length])  
        return "".join(part_1_password)

    def find_password_alternate(self) -> str:
        part_2_password = {}

        for hash_result in self.hashes_with_prefix():
            password_2_index = int(hash_result[self.prefix_length], 16)
            if password_2_index < self.password_length and password_2_index not in part_2_password.keys():
                part_2_password[password_2_index] = hash_result[self.prefix_length + 1]

            if len(part_2_password) == self.password_length:
                return "".join([part_2_password[index] for index in range(len(part_2_password))])
    

def main():
    parser = InputParser()
    secret = parser.secret

    hasher = MD5Hasher(secret=secret)

    solution_1 = hasher.find_password()
    solution_2 = hasher.find_password_alternate()
    print("The password is %s" % solution_1)
    print("The second door's password is %s" % solution_2)

if __name__ == '__main__':
    main()
