from .day_4 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_sum_of_real_room_sector_ids():
    parser = InputParser(file_name)

    id_sum = 0
    for room in parser.rooms:
        if RoomValidator.is_valid(room):
            id_sum += room.data.sector_id

    assert id_sum == 173787


def test_sector_of_north_pole_object_room():
    parser = InputParser(file_name)

    for room in parser.valid_rooms:
        if "north" in RoomDecrypter.decrypt(room):
            assert room.data.sector_id == 548
            return
