import re
import sys
from dataclasses import dataclass
from enum import Enum
from app.common import BaseInputParser


class InputParser(BaseInputParser):
    @property
    def characters(self):
        for line in self.lines:
            for character in line:
                yield character


@dataclass
class Marker:
    characters: int
    repeat: int


class DecompressorState(Enum):
    PARSING_MARKER = 0
    READY = 1


class Decompressor:
    decompressed_input = ""
    state = DecompressorState.READY

    def decompress(self, characters: str):
        tokenized_str = list(characters)
        current_character = tokenized_str.pop(0)
        token_buffer = None

        while current_character:
            if self.state == DecompressorState.READY and current_character == "(":
                self.state = DecompressorState.PARSING_MARKER
                token_buffer = ""
            elif self.state == DecompressorState.PARSING_MARKER and current_character == ")":
                self.state = DecompressorState.READY
                marker_char, marker_repeats = token_buffer.split('x')
                marker = Marker(characters=int(marker_char), repeat=int(marker_repeats))
                
                # pop for 'chars'
                chars = "".join([tokenized_str.pop(0) for _ in range(marker.characters)])
                yield marker.repeat * chars

                token_buffer = None
            elif self.state == DecompressorState.PARSING_MARKER:
                token_buffer = token_buffer + current_character
            else:
                yield current_character
           
            if len(tokenized_str) != 0:
               current_character = tokenized_str.pop(0)
            else:
                current_character = None


    def process(self, characters: str):
        self.decompressed_input = "".join(self.decompress(characters))
       
    def get_decompressed_input(self):
        return self.decompressed_input.strip()


class DecompressorV2:
    decompressed_input = ""
    length = 0

    def decompress(self, characters: str):
        state = DecompressorState.READY
        tokenized_str = list(characters)
        current_character = tokenized_str.pop(0)
        token_buffer = None
        local_count = 0

        while current_character:
            if state == DecompressorState.READY and current_character == "(":
                state = DecompressorState.PARSING_MARKER
                token_buffer = ""
            elif state == DecompressorState.PARSING_MARKER and current_character == ")":
                state = DecompressorState.READY
                marker_char, marker_repeats = token_buffer.split('x')
                marker = Marker(characters=int(marker_char), repeat=int(marker_repeats))
                
                # pop for 'chars'
                chars = "".join([tokenized_str.pop(0) for _ in range(marker.characters)])
                local_count = local_count + marker.repeat * self.decompress(chars)
                token_buffer = None
            elif state == DecompressorState.PARSING_MARKER:
                token_buffer = token_buffer + current_character
            else:
                local_count = local_count + 1
           
            if len(tokenized_str) != 0:
               current_character = tokenized_str.pop(0)
            else:
                current_character = None

        return local_count
            

    def process(self, characters: str):
        self.length = self.decompress(characters)
        #self.decompressed_input = "".join(self.decompress(characters))
       
    def get_decompressed_input(self):
        return self.length #self.decompressed_input.strip()


def main():
    parser = InputParser()
    decompressor = Decompressor()

    decompressor.process(parser.characters)

    print("Decompressed input is %s characters long." % len(decompressor.get_decompressed_input()))

    decompressorv2 = DecompressorV2()

    print(len(decompressorv2.process(parser.characters)))
    print(decompressorv2.process(parser.characters))



if __name__ == '__main__':
    main()
