from .day_9 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")

def test_decompress_no_maker():
    decompressor = Decompressor()
    decompressor.process("ADVENT")
    assert decompressor.get_decompressed_input() == "ADVENT"

def test_decompress_example_1():
    decompressor = Decompressor()
    decompressor.process("A(1x5)BC")
    assert decompressor.get_decompressed_input() == "ABBBBBC"

def test_decompress_example_2():
    decompressor = Decompressor()
    decompressor.process("(3x3)XYZ")
    assert decompressor.get_decompressed_input() == "XYZXYZXYZ"

def test_decompress_example_3():
    decompressor = Decompressor()
    decompressor.process("(6x1)(1x3)A")
    assert decompressor.get_decompressed_input() == "(1x3)A"

def test_decompress_example_4():
    decompressor = Decompressor()
    decompressor.process("A(2x2)BCD(2x2)EFG")
    assert decompressor.get_decompressed_input() == "ABCBCDEFEFG"

def test_decompress_example_5():
    decompressor = Decompressor()
    decompressor.process("X(8x2)(3x3)ABCY")
    assert decompressor.get_decompressed_input() == "X(3x3)ABC(3x3)ABCY"

def test_decompressed_input_length():
    parser = InputParser(file_name)

    decompressor = Decompressor()

    decompressor.process(parser.characters)

    assert len(decompressor.get_decompressed_input()) == 102239

def test_second_decompressor_example_1():
    decompressor = DecompressorV2()
    decompressor.process("(3x3)XYZ")
    assert decompressor.get_decompressed_input() == len("XYZXYZXYZ")

def test_second_decompressor_example_2():
    decompressor = DecompressorV2()
    decompressor.process("X(8x2)(3x3)ABCY")
    assert decompressor.get_decompressed_input() == len("XABCABCABCABCABCABCY")

def test_second_decompressor_example_3():
    decompressor = DecompressorV2()
    decompressor.process("(27x12)(20x12)(13x14)(7x10)(1x12)A")
    assert decompressor.get_decompressed_input() == 241920


def test_second_decompressor_example_4():
    decompressor = DecompressorV2()
    decompressor.process("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN")
    assert decompressor.get_decompressed_input() == 445

def test_second_decompressed_input_length():
    parser = InputParser(file_name)
    decompressor = DecompressorV2()
    decompressor.process(parser.characters)

    assert decompressor.get_decompressed_input() == 10780403063
