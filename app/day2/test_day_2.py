from .day_2 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")


def test_bathroom_layout_with_square_keypad():
    input_parser = InputParser(file_name)
    tracker = KeypadInstructionTracker()
    part_1 = ""
    for instructions in input_parser.instruction_lists:
        part_1 += str(tracker.get_position_after_instructions(instructions))

    assert part_1 == "48584"


def test_bathroom_layout_with_diamond_keypad():
    input_parser = InputParser(file_name)
    diamond_tracker = DiamondKeypadInstructionTracker()

    part_2 = ""
    for instructions in input_parser.instruction_lists:
        part_2 += str(diamond_tracker.get_position_after_instructions(instructions))

    assert part_2 == "563B6"
