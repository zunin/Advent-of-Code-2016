from dataclasses import dataclass, field
from typing import Optional, Union, List, Dict, Iterable
from app.common import BaseInputParser


@dataclass
class Chip:
    value: int


@dataclass(unsafe_hash=True)
class Bin:
    name: str


@dataclass(unsafe_hash=True)
class Bot:
    name: str
    

@dataclass
class HandoverInstruction:
    bot: Bot
    low_target: Union[Bot, Bin]
    high_target: Union[Bot, Bin]


@dataclass
class ValueInstruction:
    bot: Bot
    chip: Chip

class InputParser(BaseInputParser):
    @property
    def instructions(self):
        for line in self.lines:
            if line.startswith("value"):
                __, chip_value, _, _, _, bot_name = line.strip().split(" ")
                yield ValueInstruction(
                    chip=Chip(value=int(chip_value)), 
                    bot=Bot(name=bot_name)
                )
            else:
                _, bot_name, _, _, _, low_type, low_target, _, _, _, high_type, high_target = line.strip().split(" ")
                yield HandoverInstruction(
                    bot=Bot(name=bot_name),
                    low_target=Bin(name=low_target) if low_type == "output" else Bot(name=low_target),
                    high_target=Bin(name=high_target) if high_type == "output" else Bot(name=high_target),
                )


class FactoryControlSystem:
    state: Dict[Union[Bot, Bin], List[int]] = {}
    handover_instructions: Dict[Union[Bot, Bin], HandoverInstruction] = {}

    def process(self, instruction_generator: Iterable[Union[ValueInstruction, HandoverInstruction]]) -> Iterable[Bot]:
        instructions = list(instruction_generator)
        
        value_instructions = [instruction for instruction in instructions if isinstance(instruction, ValueInstruction)]
        handover_instructions = [instruction for instruction in instructions if isinstance(instruction, HandoverInstruction)]

        self.handover_instructions = { 
            instruction.bot: instruction for instruction in handover_instructions
        }

        for instruction in value_instructions:
            yield from self.assign_chip_to_bot(chip=instruction.chip, bot=instruction.bot)
            
    def assign_chip_to_bot(self, chip: Chip, bot: Union[Bot, Bin]) -> Iterable[Bot]:
        self.state[bot] = (self.state[bot] if bot in self.state else []) + [chip,]
        if len(self.state[bot]) == 2:
            yield from self.distribute_chips_for_bot(bot)

    def distribute_chips_for_bot(self, bot: Union[Bot, Bin]) -> Iterable[Bot]:
        lower_chip, higher_chip = sorted(self.state[bot], key=lambda c: c.value)
        
        if lower_chip.value == 17 and higher_chip.value == 61:
            yield bot

        self.state[bot] = []
        handover_instruction = self.handover_instructions[bot]
        
        self.assign_chip_to_bot(chip=lower_chip, bot=handover_instruction.low_target)
        self.assign_chip_to_bot(chip=higher_chip, bot=handover_instruction.high_target)


def main():
    pass



if __name__ == '__main__':
    main()
