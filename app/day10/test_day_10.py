from .day_10 import *
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
file_name = os.path.join(dir_path, "input.txt")



def test_second_decompressed_input_length():
    parser = InputParser(file_name)
    system = FactoryControlSystem()

    assert list(system.process(parser.instructions))[0].name == "147"

    