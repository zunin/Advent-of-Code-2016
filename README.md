[![pipeline status](https://gitlab.com/zunin/Advent-of-Code-2016/badges/master/pipeline.svg)](https://gitlab.com/zunin/Advent-of-Code-2016/commits/master) [![coverage report](https://gitlab.com/zunin/Advent-of-Code-2016/badges/master/coverage.svg)](https://gitlab.com/zunin/Advent-of-Code-2016/commits/master) [![codebeat badge](https://codebeat.co/badges/fa522fe8-a264-4eba-a0eb-997984fdaeaa)](https://codebeat.co/projects/gitlab-com-zunin-advent-of-code-2016-master)
# Advent-of-Code-2016
This my implementation of Advent of Code 2016 in Python
